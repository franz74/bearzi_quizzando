'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var ts = require('gulp-typescript');
var uglify = require('gulp-uglify');
var shell = require('gulp-shell');

gulp.task('default',['build-all'], function () {
    gulp.watch("./scss/*.scss",["sass"]);
    gulp.watch("./ts/*.ts",["typescript"]);

});
gulp.task('cordova-run-device', function ()  {
    return gulp.src('*.js', {read: false})
        .pipe(shell([
            'cordova run android --device'
        ]))
});

gulp.task('cordova-run-emulator', function ()  {
    return gulp.src('*.js', {read: false})
        .pipe(shell([
            'cordova run android --emulator'
        ]))
});

gulp.task('typescript', function() {
    var tsResult = gulp.src('ts/*.ts')
        .pipe(sourcemaps.init()) // This means sourcemaps will be generated
        .pipe(ts({
            outFile: 'bundle.js'
        }));
    return tsResult.js
        .pipe(uglify())
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest('www/js/app'));
});

gulp.task('sass', function () {
    return gulp.src('./scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./www/css/app'))
});

gulp.task('build-all',['sass','typescript']);
