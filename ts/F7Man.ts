///<reference path="../dts/framework7/framework7.d.ts"/>
///<reference path="../dts/framework7/dom7.d.ts"/>
///<reference path="../dts/Window.d.ts"/>

class F7Man {
    private myApp: Framework7;
    private mainView: Framework7.View;

    constructor() {
        console.log("F7Man");
        // Initialize app
        this.myApp = new Framework7();

// If we need to use custom DOM library, let's save it to $$ variable:
        window.$$ = Dom7;

// Add view
        this.mainView = this.myApp.addView('.view-main', {
            // Because we want to use dynamic navbar, we need to enable it for this view:
            dynamicNavbar: true
        });
    }
}
